import styles from './contact.module.css';

const ContactLinks = (props) => {
    return (
        <div className={`${props.heroContact ? styles['contact__hero'] : styles['contact']}`}>
            <ul>
                <li>
                    <a className={styles['contact__link']} href="https://linkedin.com/in/j-seppala" target="_blank" rel="noopener noreferrer" aria-label='linkedin'>
                        <i className="fa-brands fa-linkedin fa-2xl" alt='linkedin'></i>
                    </a>
                </li>
                <li>
                    <a className={styles['contact__link']} href="https://gitlab.com/jsbrew" target="_blank" rel="noopener noreferrer" aria-label='github'>
                        <i className="fa-brands fa-square-gitlab fa-2xl" alt='gitlab'></i>
                    </a>
                </li>
                <li>
                    <a className={styles['contact__link']} href="mailto:jsdev@mm.st" aria-label='email'>
                        <i className="fa-solid fa-envelope fa-2xl" alt='email'></i>
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default ContactLinks;