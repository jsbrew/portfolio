import styles from './logo.module.css';
import Rain from '../animation/rainAnimation';

const Logo = () => {
    return (
        <div className={styles['logo-container']}>
            <Rain animationDirection="left" />
            <img className={styles.logo} src="./images/green_logo.png" alt="logo" />
            <Rain animationDirection="right" />
        </div>
    )
}

export default Logo;