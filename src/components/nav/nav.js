import styles from './nav.module.css';
import heroStyles from '../hero/hero.module.css';
import aboutStyles from '../sections/about.module.css';
import projectStyles from '../sections/projects.module.css';

const scrollToElement = (elementParam) => {
    const element = document.querySelector(`.${elementParam}`);

    if (element) {
        element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    } else {
        console.warn(`Element with class '${elementParam}' not found.`);
    }
}

const Nav = () => {
    return (
        <nav className={styles.nav}>
            <div className={styles['nav-container']}>
                <a className='link' onClick={() => scrollToElement(`${heroStyles['hero-section']}`)}>
                    <img className={styles['nav-logo']} src='./images/green_logo.png' alt='logo' />
                    <small>Sanctum</small>
                </a>
                <ul className={styles.navbar}>

                    <li className="">
                        <a className='link' onClick={() => scrollToElement(`${aboutStyles['about-section']}`)}>
                            <i className='fa-solid fa-book fa-xl'  alt='about'></i>
                            <small>About</small>
                        </a>
                    </li>
                    <li className=''>
                        <a className='link' onClick={() => scrollToElement(`${projectStyles['projects-section']}`)}>
                            <i className='fa-solid fa-flask  fa-xl' alt='projects'></i>
                            <small>Projects</small>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Nav;