import styles from './cardScroller.module.css';
import skillsData from '../../data/skills';
import { useEffect } from 'react';
import interestsData from '@/data/interests';

const activateScroll = () => {
    const scrollers = document.querySelectorAll(`.${styles['scroller']}`);

    if (!window.matchMedia('(prefers-reduced-motion: reduce)').matches) {
        scrollers.forEach(scroller => {
            if (!scroller.hasAttribute('data-animated')) {
                scroller.setAttribute('data-animated', true);

                const scrollerInner = scroller.querySelector(`.${styles['scroller__inner']}`);
                const scrollerContent = Array.from(scrollerInner.children);
                const prefix = scroller.getAttribute('data-list');

                for (let i = 0; i < scrollerContent.length; i++) {
                    const duplicatedItem = scrollerContent[i].cloneNode(true);
                    const duplicatedItemClass = styles[`duplicated-${prefix}-item-${i}`];

                    duplicatedItem.setAttribute('aria-hidden', true);
                    duplicatedItem.classList.add(duplicatedItemClass);
                    scrollerInner.appendChild(duplicatedItem);
                }
            }
        });
    }
}


const CardScroller = (props) => {
    let content;

    useEffect(() => {
        activateScroll();
    }, []);

    if (props.scrollerContent === 'skills') {
        content = (
            <div className={styles['scroller']} data-list="skills" data-direction="left">
            <ul className={`${styles['badges']} ${styles['skill-badges']} ${styles['scroller__inner']}`}>
                {
                    skillsData.map((skill, index) => (
                        <li key={index}>
                            <div className={styles['badge']}>
                                <div className={styles['badge__logo']}>
                                    <i className={skill.icon} style={{ color: skill.color }}></i>
                                </div>
                                <span className={styles['badge__name']}>{skill.name}</span>
                            </div>
                        </li>
                    ))
                }
            </ul>
        </div>
        )
    }
    else if (props.scrollerContent) {
        content = (
            <div className={styles['scroller']} data-list="interests" data-direction="right">
            <ul className={`${styles['badges']} ${styles['interests-badges']} ${styles['scroller__inner']}`}>
                {
                    interestsData.map((interest, index) => (
                        <li key={index}>
                            <div className={styles['badge']}>
                                <div className={styles['badge__logo']}>
                                    <i className={interest.icon} style={{ color: interest.color }}></i>
                                </div>
                                <span className={styles['badge__name']}>{interest.name}</span>
                            </div>
                        </li>
                    ))
                }
            </ul>
        </div>
        )
    }

    return (
        <>
            {content}
        </>
    )
}

export default CardScroller;
