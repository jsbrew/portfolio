import styles from './card.module.css';
import CardText from './cardText';
import CardScroller from './cardScroller';
import CardTimeline from './cardTimeline';
import projectsData from '../../data/projects';
import formStyles from '../form/form.module.css';

const scrollToElement = (elementParam) => {
    const element = document.querySelector(`.${elementParam}`);

    if (element) {
        element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    } else {
        console.warn(`Element with class '${elementParam}' not found.`);
    }
}

const scrollToProject = (elementId) => {
    const element = document.querySelector(`#${elementId}`);
    element.scrollIntoView({behavior: 'smooth'});
}

const Card = (props) => {
    const HeadingTag = props.headingSize || 'h1';

    return (
        <div className={`${styles['card-wrapper']} ${props.heroCard ? styles['order-card'] : ''}`}>
            <div className={`${styles.card} ${props.glassCard ? styles['card__glass'] : ''}`}>
                {props.emblem ? (
                    <div className={styles['card__emblem-wrapper']}>
                        <div className={styles['card__emblem-bg']}>
                            <div className={styles['card__emblem']}>
                                <i className={props.emblemIcon}></i>
                            </div>
                        </div>
                    </div>
                ) : null}
                <div className={styles['card__heading']}>
                    <HeadingTag>
                        {props.cardHeading}
                    </HeadingTag>
                </div>
                <div className={styles['card__body']}>
                    {props.cardContentType === 'text' ? (
                        <CardText cardContent={props.cardText} />
                    ) : props.cardContentType === 'scroller' ? (
                        <CardScroller scrollerContent={props.cardScrollContent} />
                    ) : props.cardContentType === 'timeline' ? (
                        <CardTimeline timelineContent={props.cardTimelineContent} />
                    ) : (
                        null
                    )}
                </div>
                <div className={styles['card__footer']}>
                    {props.cardButton && <a className='btn' onClick={() => scrollToElement(`${formStyles['form-section']}`)}>{props.buttonText}</a>}
                    {props.projectButtons && projectsData.map((project, index) =>
                        <button className='btn' onClick={() => scrollToProject(project.index)} key={index}>{project.title}</button>)}
                    {props.projectSource && 
                    <a className='link' href={props.projectSource} target="_blank" rel="noopener noreferrer">
                        Source
                        <i className="fa-brands fa-git-alt fa-lg"></i>
                    </a>}
                    {props.projectVisit &&
                    <a className='link' href={props.projectVisit} target="_blank" rel="noopener noreferrer">
                        Visit
                        <i className="fa-solid fa-eye"></i>
                    </a>}
                </div>
            </div>
        </div>
    )
}

export default Card;