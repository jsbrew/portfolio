import styles from './cardTimeline.module.css';

const CardTimeline = (props) => {
    const listItems = props.timelineContent;

    return (
        <ul className={styles['exp-list']}>
            {listItems.map((item, index) => (
                <li key={index}>
                    <div>
                        <small>{item.years}</small>
                    </div>
                    <div>
                        <h5>{item.title}</h5>
                        <small>{item.desc}</small>
                    </div>
                </li>
            ))}
        </ul>
    )
}

export default CardTimeline;