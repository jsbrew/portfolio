const CardText = (props) => {
    return (
        <p>
            {props.cardContent}
        </p>
    )
}

export default CardText;