import ContactLinks from '../contact/contact';
import styles from './footer.module.css';
import React, { useEffect, useState } from "react";

const getYear = () => {
    const date = new Date();
    const year = date.getFullYear();

    return year.toString();
}

const Footer = () => {
    const [currentYear, setCurrentYear] = useState('');

    useEffect(() => {
        setCurrentYear(getYear());
    }, []);

    return (
        <section className={styles['footer-section']}>
            <div className={`container ${styles['footer']}`}>
                <div className={styles['footer__top']}>

                </div>
                <div className={styles['footer__bottom']}>
                    <ContactLinks />
                    <div>
                        <small>@ <span className={styles['footer__year']}>{currentYear}</span> JS | PORTFOLIO</small>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Footer;