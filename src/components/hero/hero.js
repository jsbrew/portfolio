import styles from './hero.module.css';
import Card from '../card/card';
import ContactLinks from '../contact/contact';
import Logo from '../logo/logo';

const Hero = () => {
    return (
        <section className={styles['hero-section']}>
            <div className={`container ${styles.hero}`}>
                <div className='cards'>

                    <Card
                        headingSize="h1"
                        cardHeading="Crafting Code & Brewing Experiences"
                        cardContentType="text"
                        cardText="Where web development meets innovation. Welcome to my world of digital creations.
                        Explore, experience, and embark on a journey through the web."
                        cardTitleHighlight={['code', 'experiences']}
                        cardButton={true}
                        heroCard={true}
                        buttonText="Let's Design Magic"
                    />

                    <Logo />

                    <ContactLinks heroContact={true} />
                </div>
            </div>
        </section>
    )
}

export default Hero;