import styles from './about.module.css';
import Card from '../card/card';

const About = () => {
    return (
        <section className={styles['about-section']}>
            <div className={`container ${styles['about']}`}>
                <div className="cards">
                    <Card
                        headingSize="h2"
                        cardHeading="Skill Stack"
                        cardContentType="scroller"
                        cardScrollContent="skills"
                        glassCard={true}
                        emblem={true}
                        emblemIcon="fa-solid fa-chart-line fa-xl"
                    />

                    <Card
                        headingSize="h2"
                        cardHeading="Experience"
                        cardContentType="timeline"
                        cardTimelineContent={[{'years': '2022-2023', 'title': 'Liana Technologies', 'desc': 'Junior Front-End Developer'},
                        {'years': '2009-2023', 'title': 'Securitas', 'desc': 'Healthcare Security'}
                        ]}
                        glassCard={true}
                        emblem={true}
                        emblemIcon="fa-solid fa-layer-group fa-xl"
                    />

                    <Card
                        headingSize="h2"
                        cardHeading="Eductation"
                        cardContentType="timeline"
                        cardTimelineContent={[{'years': '2020-2024', 'title': 'Oulu University of Applied Sciences', 'desc': "Bachelor's Degree"},
                        {'years': '2004-2008', 'title': 'Lahden yhteiskoulu', 'desc': 'High School'}
                        ]}
                        glassCard={true}
                        emblem={true}
                        emblemIcon="fa-solid fa-graduation-cap fa-xl"
                    />

                    <Card
                        headingSize="h2"
                        cardHeading="Interests"
                        cardContentType="scroller"
                        cardScrollContent="interests"
                        glassCard={true}
                        emblem={true}
                        emblemIcon="fa-solid fa-shield-heart fa-xl"
                    />

                </div>
            </div>
        </section>
    )
}

export default About;