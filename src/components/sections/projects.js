import Card from '../card/card';
import projectsData from '../../data/projects';
import styles from './projects.module.css';

const Projects = () => {
    return (
        <section className={styles['projects-section']}>
            <div className={`container ${styles['interblock']}`}>
                <div className="cards">
                <Card
                        headingSize="h2"
                        cardHeading="Projects Brewed by a Digital Alchemist"
                        cardContentType="text"
                        cardText="Explore my Projects to witness the growth of a developer in action. 
                        From early experiments to polished creations, each project marks a step forward in my coding journey."
                        projectButtons={true}
                    />
                    <ul className={styles['interblock__snap-container']}>
                        {
                            projectsData.map((project, index) => (
                                <li className={styles['interblock__snap-item']} id={project.index} key={project.index}>
                                    <Card
                                        headingSize="h3"
                                        cardHeading={project.title}
                                        cardContentType="text"
                                        cardText={project.description}
                                        glassCard={true}
                                        projectSource={project.source}
                                        projectVisit={project.view}
                                        emblem={true}
                                        emblemIcon="fa-regular fa-folder-open fa-xl"
                                    />
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
        </section>
    )
}

export default Projects;