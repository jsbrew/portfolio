import styles from './reference.module.css';

const Reference = (props) => {
    return (
        <section className={`section ${styles['ref-section']}`}>
            <div className={`container ${styles['ref-container']}`}>
                <div className={styles['ref-box']}>
                    <div className={styles['ref-title']}>
                        <h2>
                            {props.heading}
                        </h2>
                    </div>
                    <div className={styles['ref-description']}>
                        {props.paragraphs.map((paragraph, index) => (
                            <small key={index}>
                                {paragraph}
                            </small>
                        ))}
                    </div>
                    <div className={styles['ref-btns']}>
                        <a className="link" href={props.link} target="_blank" rel="noopener noreferrer">
                            {props.linkText}

                        </a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Reference;