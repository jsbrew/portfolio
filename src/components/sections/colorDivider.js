import { useEffect } from 'react';
import styles from './colorDivider.module.css';
import formStyles from '../form/form.module.css';

const scrollToElement = (elementParam) => {
    const element = document.querySelector(`.${elementParam}`);

    if (element) {
        element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    } else {
        console.warn(`Element with class '${elementParam}' not found.`);
    }
}

const slideFeature = () => {
    const elements = document.querySelectorAll(`.${styles.divider}`);
    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            entry.target.classList.toggle(styles.show, entry.isIntersecting);
        }, {threshold: 1});
    });

    elements.forEach((element, index) => {
        observer.observe(element, { key: index });
    })

}

const ColorDivider = (props) => {
    const HeadingTag = props.headingSize ?? 'h2';

    useEffect(() => {
        slideFeature();
    }, [])

    return (
        <section className={`${styles['divider-section']} ${props.skew === 'left' ? styles['divider-skew-left'] : styles['divider-skew-right']}`}>
            <div className={`container ${styles.divider}`}>
                <div className={styles['divider__heading']}>
                    <HeadingTag>
                        {props.dividerHeading}
                    </HeadingTag>
                </div>
                <div className={styles['divider__body']}>
                    <p>
                        {props.dividerText}
                    </p>
                </div>
                <div className={styles['divider__footer']}>
                    {props.dividerButton && <a className='btn' onClick={() => scrollToElement(`${formStyles['form-section']}`)}>{props.buttonText}</a>}
                </div>
            </div>
        </section>
    )
}

export default ColorDivider;