import { useEffect, useState } from 'react';
import styles from './buttonMenu.module.css';
import heroStyles from '../hero/hero.module.css';
import aboutStyles from '../sections/about.module.css';
import projectStyles from '../sections/projects.module.css';
import navStyles from '../nav/nav.module.css';

const scrollToElement = (elementParam) => {
    const element = document.querySelector(`.${elementParam}`);

    if (element) {
        element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    } else {
        console.warn(`Element with class '${elementParam}' not found.`);
    }
}
 
const ButtonMenu = () => {
    const [menuOpen, setMenuOpen] = useState(false);

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    }

    const calculateItemPosition = (index) => {
        return menuOpen ? `${5 + index * 5}rem` : '0rem';
    }

    const buttonRotation = {
        transform: menuOpen ? 'rotate(-135deg)' : 'rotate(0deg)',
        transition: 'transform 0.3s ease'
    };

    const slideFeature = () => {
        const elements = document.querySelectorAll(`.${navStyles.nav}`);
        const menu = document.querySelector(`.${styles['btn-menu']}`);
        const observer = new IntersectionObserver(entries => {

            if (entries[0].isIntersecting) {
                setMenuOpen(false);
            }
            
            menu.classList.toggle(styles.show, !entries[0].isIntersecting);
        });
    
        elements.forEach(element => {
            observer.observe(element);
        });
    }

    useEffect(() => {
        slideFeature();
    }, [])

    return (
        <div className={`container ${styles['btn-menu-container']}`} >
            <ul className={styles['btn-menu']}>
                <li className={styles['btn-menu__item']} style={{ right: calculateItemPosition(2) }}>
                    <a className='link' onClick={() => scrollToElement(`${heroStyles['hero-section']}`)}>
                        <i className='fa-solid fa-circle-chevron-up fa-2xl'></i>
                    </a>
                </li>
                <li className={styles['btn-menu__item']} style={{ right: calculateItemPosition(1) }}>
                    <a className='link' onClick={() => scrollToElement(`${aboutStyles['about-section']}`)}>
                        <i className='fa-solid fa-book fa-2xl'></i>
                    </a>
                </li>
                <li className={styles['btn-menu__item']} style={{ right: calculateItemPosition(0) }}>
                    <a className='link' onClick={() => scrollToElement(`${projectStyles['projects-section']}`)}>
                        <i className='fa-solid fa-flask  fa-2xl'></i>
                    </a>
                </li>
                <li className={styles['btn-menu__item']}>
                    <a className='link' onClick={toggleMenu}>
                        <i className='fa-solid fa-circle-plus fa-2xl' style={buttonRotation }></i>
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default ButtonMenu;