import Nav from './nav/nav';
import Hero from './hero/hero';
import About from './sections/about';
import ColorDivider from './sections/colorDivider';
import Projects from './sections/projects';
import Footer from './footer/footer';
import ButtonMenu from './menu/buttonMenu';
import Form from './form/form';
import Head from 'next/head';
import Reference from './sections/reference';

const App = () => {
    return (
        <>
            <Head>
                <title>JS | Portfolio</title>
            </Head>
            <header>
                <Nav />
            </header>
            <main>
                <Hero />
                <Reference 
                    heading="Professional Acknowledgment"
                    paragraphs={['"I worked with Juuso in a customer project where we developed an event section as part of a larger website.',
                    "It came out great, delivering event app like experience while saving customer's human resources and lower their cost related to this big yearly event.",
                    'What set Juuso apart was that he did not only code the minimum that was in the spec but put his thought and own touch to it, giving that wow effect. I highly recommend Juuso!"',
                    "- Key Account Manager at Liana Technologies"
                    ]}
                    linkText="To linkedIn"
                    link="https://linkedin.com/in/j-seppala"
                />
                <About />
                <ColorDivider 
                    headingSize="h2"
                    dividerHeading="Digital Alchemist: Crafting Ideas into Web Realities"
                    dividerText="A front-end developer with a passion for turning ideas into digital realities.
                    My journey in web development is defined by my creative approach to problem-solving,
                    unwavering perseverance to overcome challenges, and a perfectionist's eye for detail."
                    dividerButton={true}
                    buttonText="Let's Brew Ideas"
                    skew="right"
                />
                <Projects />
                <ColorDivider 
                    headingSize="h2"
                    dividerHeading="Apprentice of Cyber Sorcery"
                    dividerText="As an aspiring cybersecurity enthusiast, 
                    I embark on a journey of exploration into the fascinating realms of ethical hacking,
                    where each challenge becomes an opportunity to enhance my skills and understanding"
                    dividerButton={true}
                    buttonText="Let's cast spells"
                    skew="left"
                />
                <Form />
                <Footer />
                <ButtonMenu />
            </main>
        </>
    )
}

export default App;