import { useState, useRef } from 'react';
import styles from './form.module.css';

const Form = () => {
    const formRef = useRef();
    const modalRef = useRef();

    const openModal = () => {
        modalRef.current.showModal();
    }

    const closeModal = () => {
        modalRef.current.close();
    }

    const isFormFilled = () => {
        const inputs = formRef.current.querySelectorAll('input');
        return Array.from(inputs).every(input => input.value.trim() !== '');
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        const myForm = event.target;
        const formData = new FormData(myForm);

        if (isFormFilled()) {
            fetch("/", {
                method: "POST",
                headers: { "Content-Type": "application/x-www-form-urlencoded" },
                body: new URLSearchParams(formData).toString(),
            })
                .then(() => {
                    openModal();
                    formRef.current.reset();
                })
                .catch((error) => alert(error));
        }
        else {
            alert("Please fill the form.")
        }
};

return (
    <section className={styles['form-section']}>
        <div className={`container ${styles['form-container']}`}>
            <div className={styles['form-heading']}>
                <h2>LET&rsquo;S BREW IDEAS</h2>
            </div>
            <form className={styles['form']} name="contact" ref={formRef} data-netlify="true" data-netlify-honeypot="bot-field" onSubmit={handleSubmit}>
                <input type="hidden" name="form-name" value="contact" />
                <div className={styles['form__field']}>
                    <label>Your Name</label>
                    <input type="text" name="name" placeholder="Enter your name" required/>
                </div>
                <div className={styles['form__field']}>
                    <label>Email</label>
                    <input type="email" name="email" placeholder="Enter your email address" required/>
                </div>
                <div className={styles['form__field']}>
                    <label>Message</label>
                    <textarea type="textarea" name="text" placeholder="Share your thoughts..." required></textarea>
                </div>
                <div className={styles['form__field']}>
                    <button className='btn' type="submit">
                        Submit
                        <i className="fa-solid fa-arrow-right-long"></i>
                    </button>
                </div>
            </form>
        </div>

        <dialog className={styles['modal']} id="modal" ref={modalRef}>
            <h2>Data transmutation complete!</h2>
            <p>Your input has been successfully processed in the digital alchemy laboratory.</p>
            <button className="btn" onClick={() => closeModal()}>Close</button>
        </dialog>
    </section>
)
}

export default Form;