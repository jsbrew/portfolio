import { shapes } from "@/data/rainShapes";
import styles from './rainAnimation.module.css';

const Rain = (props) => {
    return (
        <div className={styles['bga-container']}>
        {
            shapes.map(shape => (
              <div key={shape.id} className={`${props.animationDirection === 'left' ? styles['shape-frame-left'] : styles['shape-frame-right']}`} style={{animationDelay: shape.delay}}>
                <div 
                  className={styles['shape']} 
                  style={{height: shape.height, width: shape.width, animationDelay: shape.delay}}>
                </div>
              </div>
            ))
        }
    </div>
    )
}

export default Rain;