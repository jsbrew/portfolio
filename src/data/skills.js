const skillsData = [
  { "name": "#JavaScript", "icon": "fa-brands fa-square-js fa-2xl", "color": "#f5f52a" },
  { "name": "#HTML", "icon": "fa-brands fa-html5 fa-2xl", "color": "#f06529" },
  { "name": "#CSS", "icon": "fa-brands fa-css3-alt fa-2xl", "color": "#0088f7" },
  { "name": "#SCSS", "icon": "fa-brands fa-sass fa-2xl", "color": "#CC699C" },
  { "name": "#Bootstrap", "icon": "fa-brands fa-bootstrap fa-2xl", "color": "#5c3566" },
  { "name": "#Python", "icon": "fa-brands fa-python fa-2xl", "color": "#FFD43B" },
  { "name": "#React", "icon": "fa-brands fa-react fa-2xl", "color": "#61dbfb" },
  { "name": "#Git", "icon": "fa-brands fa-square-git fa-2xl", "color": "#f34f29" },
  { "name": "#WordPress", "icon": "fa-brands fa-wordpress fa-2xl", "color": "#096484" },
  { "name": "#Linux", "icon": "fa-brands fa-linux fa-2xl", "color": "#000000" },
  { "name": "#NodeJS", "icon": "fa-brands fa-node fa-2xl", "color": "#3c873a" },
  { "name": "#TypeScript", "icon": "fa-solid fa-code fa-2xl", "color": "#007acc" }
];

export default skillsData;