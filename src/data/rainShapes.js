export const shapes = [
    { id: 1, height: '0.5rem', width: '0.5rem', delay: '15s' },
    { id: 2, height: '1.5rem', width: '1.5rem', delay: '10s' },
    { id: 3, height: '1rem', width: '1rem', delay: '2s' },
    { id: 4, height: '0.4rem', width: '0.4rem', delay: '12s' },
    { id: 5, height: '1rem', width: '1rem', delay: '3s' },
    { id: 6, height: '1.2rem', width: '1.2rem', delay: '5s' },
    { id: 7, height: '1.5rem', width: '1.5rem', delay: '1.5s' },
    { id: 8, height: '1rem', width: '1rem', delay: '17s' },
    { id: 9, height: '0.3rem', width: '0.3rem', delay: '3s' },
    { id: 10, height: '0.3rem', width: '0.3rem', delay: '0s' },
    { id: 11, height: '0.6rem', width: '0.6rem', delay: '13s' },
    { id: 12, height: '1rem', width: '1rem', delay: '4s' },
    { id: 13, height: '0.8rem', width: '0.8rem', delay: '7s' },
    { id: 14, height: '0.4rem', width: '0.4rem', delay: '3s' },
    { id: 15, height: '1rem', width: '1rem', delay: '1s' },
    { id: 16, height: '1.2rem', width: '1.2rem', delay: '9s' },
    { id: 17, height: '0.5rem', width: '0.5rem', delay: '6s' },
    { id: 18, height: '0.8rem', width: '0.8rem', delay: '2s' },
    { id: 19, height: '0.5rem', width: '0.5rem', delay: '1s' },
    { id: 20, height: '0.3rem', width: '0.3rem', delay: '0s' },
];
