const interestsData = [
  { "name": "#CyberSec", "icon": "fa-solid fa-fingerprint fa-2xl", "color": "#00b7eb" },
  { "name": "#Thinkpads", "icon": "fa-solid fa-laptop fa-2xl", "color": "#3C873A" },
  { "name": "#Philosophy", "icon": "fa-regular fa-lightbulb fa-2xl", "color": "#800080" },
  { "name": "#Linux", "icon": "fa-brands fa-linux fa-2xl", "color": "#000000" },
  { "name": "#Psychology", "icon": "fa-solid fa-brain fa-2xl", "color": "#ffd700" },
];
  
  export default interestsData;