const projectsData = [
    {
        "title": "CustomTemp",
        "description": "Custom template made with React, Typescript and NextJS. Under development.",
        "source": "https://gitlab.com/jsbrew/yuso-custom-template",
        "view": "https://yuso-custom-temp.netlify.app",
        "index": "pro-1"
    },
    {
        "title": "CoinFlask",
        "description": "A personal project aimed at creating a simple app for tracking cryptocurrency prices. Currently under construction, this project is a labor of love aimed at providing a straightforward tool for staying informed about digital asset prices.",
        "source": "https://gitlab.com/jsbrew/coin-flask",
        "view": "https://coinflask.netlify.app",
        "index": "pro-2"
    },
    {
        "title": "Kofeiinikomppania",
        "description": "Back in 2021, a school project brought to life an e-commerce site using WordPress and WooCommerce. Explore the online store and join in on this digital shopping experience.",
        "source": null,
        "view": "https://kauppa.kofeiinikomppania.com",
        "index": "pro-3"
    },
    {
        "title": "CompanyTemp",
        "description": "Traditional template made with React, Typescript and NextJS. Under development.",
        "source": "https://gitlab.com/jsbrew/company-template",
        "view": "https://corp-template.netlify.app",
        "index": "pro-4"
    },
    {
        "title": "ProSignum",
        "description": "Step into the ancient world of secret messages on this React-based website! Explore classic encryption methods and uncover the secrets of the past in a modern interface.",
        "source": "https://gitlab.com/jsbrew/signum",
        "view": "https://prosignum.github.io",
        "index": "pro-5"
    },
    {
        "title": "Liana Technologies",
        "description": "Highlighting my HTML and CSS expertise, I crafted a sleek web page for Liana. The project excelled in design and device responsiveness, emphasizing my dedication to creating a user-friendly experience. Note: The news feature may not work due to the current state of my Heroku server.",
        "source": "https://gitlab.com/jsbrew/liana",
        "view": "https://lianatech.netlify.app",
        "index": "pro-6"
    },
    {
        "title": "Digimoguli",
        "description": "Showcasing my skills in HTML and CSS, delivering a polished web application that excelled in both design and device responsiveness. The project highlighted my commitment to creating seamless and visually appealing user experiences.",
        "source": "https://gitlab.com/jsbrew/digimoguli",
        "view": "https://digimoguli.netlify.app",
        "index": "pro-7"
    },
];

export default projectsData;